# [Portainer](https://www.portainer.io)

A centralized service delivery platform for containerized apps

## Requirements

- [Docker](https://docs.docker.com/install)
- [Docker Compose](https://docs.docker.com/compose/install)

## How to run it?

- Clone the repository
- Run the application

```bash
docker-compose up
```